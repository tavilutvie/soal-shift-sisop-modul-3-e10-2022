#include <stdio.h>
#include <ctype.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <curl/curl.h>


/*
    check existingfile
*/
int cfileexists(const char *file)
{
    struct stat buff;
    int exist = stat(file, &buff);
    if (exist != 0)
        return 0;
    else
        return 1;
}

void *move(void *filename)
{
    char cwd[PATH_MAX];
    char dirname[2000], hidden[1000], hiddenname[1000], file[1000], existsfile[1000];
    int i;
    strcpy(existsfile, filename);
    strcpy(hiddenname, filename);
    char *name = strrchr(hiddenname, '/');
    strcpy(hidden, name);

    /*
        hidden file , if start with (.).
    */
    if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
    /*
        file with extension
    */
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        /*
            to insensitive
        */
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
    /*
        file doesnt have extension
    */
    else
    {
        strcpy(dirname, "Unknown");
    }

    /*
        check existing file, 
        if not exist, create directory
    */
    int exist = cfileexists(existsfile);
    if (exist)
        mkdir(dirname, 0777);

    /*
        get filename
    */
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char namafile[2000];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        /*
        movefile
        */
        rename(filename, namafile);
    }
}

void rekursif(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir){
        remove(basePath);
        return;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            /*

            */
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                /*

                */
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }

            rekursif(path);
        }
    }
    closedir(dir);
}



int main(){

    //system("unzip hartakarun.zip");
    FILE * file;
    char buff[100];
    int read;

    memset(buff, '\0', sizeof(buff));
    file = popen("unzip -o hartakarun.zip", "r");

    if( file != NULL){
        read = fread(buff, sizeof(char), 100, file);
    }

    if (read > 0){
        printf("%s\n", buff);
    }
    pclose(file);


    if(chdir("./hartakarun") < 0){
        exit(EXIT_FAILURE);
    }
    

    char *curr_dir = (char*) malloc (1024);
    getcwd(curr_dir, 1024);
        //printf("%s\n", curr_dir);
    /*
    if(getcwd(curr_dir, 1024) != NULL){
    }
    */
    rekursif(curr_dir);

    //system("zip -j -r hartakarun.zip hartakarun");
    return 0;
}