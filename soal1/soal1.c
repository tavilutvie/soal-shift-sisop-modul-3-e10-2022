#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <memory.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <syslog.h>
#include <dirent.h>
#include <pwd.h>

char map_decode[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
char res[512];
pthread_t thread[12];

void *file_download(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, thread[0]))
  {
    int stat;
    if (fork() == 0)
    {
      char *cmd[] = {"mkdir", "-p", "music", NULL};
      execv("/usr/bin/mkdir", cmd);
    }
    else
    {
      while ((wait(&stat)) > 0)
        ;
      int stat2;

      if (fork() == 0)
      {
        char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/usr/bin/wget", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"unzip", "-q", "music.zip", "-d", "music", NULL};
        execv("/usr/bin/unzip", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;
    }
  }
  else if (pthread_equal(id, thread[1]))
  {

    int stat;
    if (fork() == 0)
    {
      char *cmd[] = {"mkdir", "-p", "quote", NULL};
      execv("/usr/bin/mkdir", cmd);
    }
    else
    {
      while ((wait(&stat)) > 0)
        ;
      int stat2;
      if (fork() == 0)
      {
        char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/usr/bin/wget", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"unzip", "-q", "quote.zip", "-d", "quote", NULL};
        execv("/usr/bin/unzip", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;
    }
  }
}

void decode(char *txt)
{
  char buff[4], result[100];
  int i = 0, index = 0, count = 0, j;

  while (txt[i] != '\0')
  {
    for (j = 0; j < 64 && map_decode[j] != txt[i]; j++);
    buff[count++] = j;
    if (count == 4)
    {
      result[index++] = (buff[0] << 2) + (buff[1] >> 4);
      if (buff[2] != 64)
        result[index++] = (buff[1] << 4) + (buff[2] >> 2);
      if (buff[3] != 64)
        result[index++] = (buff[2] << 6) + buff[3];
      count = 0;
    }
    i++;
  }

  result[index] = '\0';
  strcpy(res, result);
}

void *decodefile(void *arg)
{
  pthread_t id = pthread_self();

  if (pthread_equal(id, thread[0]))
  {
    DIR *dir;
    struct dirent *d1;
    dir = opendir("./music/");

    while ((d1 = readdir(dir)) != NULL)
    {
      if (strstr(d1->d_name, ".txt"))
      {
        char file[150], loc[150], buffer[150];
        strcpy(file, "./music/");
        strcat(file, d1->d_name);
        FILE *hasil = fopen(file, "r");
        if (hasil == NULL)
            continue;
        fgets(buffer, 150, hasil);
        fclose(hasil);
        decode(buffer);
        FILE *file_hasil = fopen("./hasil/music.txt", "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
      }
    }
    closedir(dir);
  }
  else if (pthread_equal(id, thread[1]))
  {

    DIR *dir;
    struct dirent *d2;
    dir = opendir("./quote/");
    while ((d2 = readdir(dir)) != NULL)
    {
      if (strstr(d2->d_name, ".txt"))
      {
        char file[150];
        char loc[150], buffer[150];
        strcpy(file, "./quote/");
        strcat(file, d2->d_name);
        FILE *hasil = fopen(file, "r");
        if (hasil == NULL)
          continue;
        fgets(buffer, 150, hasil);
        fclose(hasil);
        decode(buffer);
        FILE *file_hasil = fopen("./hasil/quote.txt", "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
      }
    }
    closedir(dir);
  }
}

void *addtozip(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, thread[0]))
  {
    if (fork() == 0)
    {
      char *cmd[] = {"unzip", "-P", "mihinomenestmilkyway", "-q", "hasil.zip", NULL};
      execv("/usr/bin/unzip", cmd);
    }
  }
  else if (pthread_equal(id, thread[1]))
  {
    FILE *file_no = fopen("./hasil/no.txt", "a");
    fprintf(file_no, "No");
    fclose(file_no);
  }
}

void filezip()
{
  int stat;
  if (fork() == 0)
  {
    char *cmd[] = {"zip", "-P", "mihinomenestmilkyway", "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&stat)) > 0);
}

int main(int argc, char const *cmd[])
{
  int err;
  for (int i = 0; i < 2; i++)
    err = pthread_create(&(thread[i]), NULL, &file_download, NULL);
  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);

  for (int i = 0; i < 2; i++)
    err = pthread_create(&(thread[i]), NULL, &decodefile, NULL);
  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);

  filezip();

  for (int i = 0; i < 2; i++)
    err = pthread_create(&(thread[i]), NULL, &addtozip, NULL);
  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);

  filezip();

  return 0;
}
