#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int checkusername(char* username)
{
    FILE* fp = fopen("users.txt", "r");
    char* line = NULL;
    size_t len = 0;
    ssize_t read;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char* token = strtok(line, ":");
        if (strcmp(token, username) == 0)
        {
            return 1;
        }
    }
    fclose(fp);
    return 0;
}
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    char input[1024];
	while (1){	
        char username[1024], password[1024];
        bool valid1=false, valid2=false, valid3=false;
		valread = read(sock, buffer, 1024);
		printf("%s", buffer);
        bzero(buffer, 1024);
        bzero(input, 1024);
		scanf(" %[^\n]s", input);
        write(sock, input, strlen(input));
        if (strcmp(input, "register")==0){
            valread = read(sock, buffer, 1024);
		    printf("%s", buffer);
            scanf(" %[^\n]s", username);
            
            if(checkusername(username)==1){
                printf("Username already exists\n");
                break;
            }
            else{
                write(sock, username, 1024);
                valread = read(sock, buffer, 1024);
                while(1){
                    printf("%s", buffer); 
                    scanf(" %[^\n]s", password);

                    int i=0;
                    while(i < strlen(password)) {
                        if(password[i] >= '0' && password[i] <= '9') valid1 = true;
                        if(password[i] >= 'A' && password[i] <= 'Z') valid2 = true;
                        if(password[i] >= 'a' && password[i] <= 'z') valid3 = true;
                        i++;
                    }

                    if(strlen(password)>5 && valid1 && valid2 && valid3){
                        valid1=false;
                        valid2=false;
                        valid3=false;
                        break;
                    }
                    else{
                        valid1=false;
                        valid2=false;
                        valid3=false;                
                    }
                }
                write(sock, password, 1024);
            }
        }    
        else if (strcmp(input, "login")==0){
            while (1){
                char ver[1024] = {0};
                while (strcmp(ver, "") == 0)
                {
                    bzero(buffer, 1024);
                    valread = read(sock, buffer, 1024);
                    printf("%s", buffer);
                    scanf(" %[^\n]s", username);
                    write(sock, username, 1024);

                    bzero(buffer, 1024);
                    valread = read(sock, buffer, 1024);
                    printf("%s", buffer); 
                    scanf(" %[^\n]s", password);
                    write(sock, password, 1024);

                    valread = read(sock, ver, 1024);
                }
                while (!strcmp(ver, "1")){
                    bzero(buffer, 1024);    
                    valread = read(sock, buffer, 1024);
                    printf("%s", buffer);
                    bzero(buffer, 1024);
                    scanf(" %[^\n]s", input);
                    write(sock, input, 1024);
                    if (!strcmp(input, "add")){
                        return 0;
                    }
                }
            }

        }
        if (strcmp(input, "exit")==0){
			return 0;
		}  
    }
    return 0;
}
	