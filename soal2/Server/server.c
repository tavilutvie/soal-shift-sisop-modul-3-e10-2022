#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h> 
#include <sys/types.h>
#include <sys/stat.h>
#define PORT 8080


void *fungsi(void *socket_desc)
{
	int sock = *(int*)socket_desc;
	int valread;
	char menu[1024] = {0}, buffer[1024] = {0}, username[1024] = {0}, password[1024] = {0}, input[1024] = {0};
	FILE* fp;
	fp = fopen("problems.tsv", "a");
	fclose(fp);
	while (1)
	{
		sprintf(menu, "register / login / exit\n\e[s\n\e[u");
		write(sock, menu, 1024); 
		bzero(menu, 1024);
		valread = read(sock, input, 1024); 
		if (valread < 1) return 0;

		if (strcmp(input, "register")==0){
			FILE* fp = fopen("users.txt", "a");
			sprintf(menu, "Enter username: \e[s\n\e[u");
			write(sock, menu, 1024); 
			bzero(menu, 1024);
			valread = read(sock, username, 1024);
			if (valread < 1) return 0;

			sprintf(menu, "Enter password : \e[s\n\e[u");
			write(sock, menu, 1024); 
			bzero(menu, 1024);
			valread = read(sock, password, 1024);
			if (valread < 1) return 0;

			fprintf(fp, "%s:%s\n", username, password);
			fclose(fp);
		}
		else if (strcmp(input, "login")==0)
		{
			char* ver = "0";
			FILE* fp = fopen("users.txt", "r+");
			while (ver == "0"){
				bzero(menu, 1024);
				sprintf(menu, "Enter username : \e[s\n\e[u");
				int ler = write(sock, menu, 1024);
				bzero(menu, 1024);
				valread = read(sock, username, 1024);
				if (valread < 1) return 0;

				sprintf(menu, "Enter password : \e[s\n\e[u");
				write(sock, menu, 1024); 
				bzero(menu, 1024);
				valread = read(sock, password, 1024); 
				if (valread < 1) return 0;

				while (fgets(buffer, 1024, fp) != NULL && ver == "0") 
				{
					char f_username[1024], f_password[1024];
					char *token = strtok(buffer, ":");
					strcpy(f_username, token);
					token = strtok(NULL, "\n");
					strcpy(f_password, token);
					
					if (strcmp(username, f_username) == 0 && strcmp(password, f_password) == 0)
					{
						printf("Logged in\n");
						ver = "1";
						write(sock, ver, 1024);
					}
				}	
			}
			while(ver == "1"){
				sprintf(menu, "add / see / download <judul-problem> / submit <judul-problem> / exit \e[s\n\e[u");
				write(sock, menu, 1024);
				valread = read(sock, input, 1024);
				if (!strcmp(input, "add")){
					return 0;
				}
			}
		}
		if (strcmp(input, "exit")==0){
			return 0;
		}

	}

	free(socket_desc);
	return 0;
}

int main(int argc, char const *argv[]) {

    FILE* fp = fopen("users.txt","a");
    fclose(fp);

    int server_fd, new_socket, *new_sock;
    struct sockaddr_in server, client;
    int opt = 1;
    int addrlen = sizeof(server);
    char buffer[1024] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&server, sizeof(server))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while( (new_socket = accept(server_fd, (struct sockaddr *)&client, (socklen_t*)&addrlen)) )
	{
		pthread_t sniffer_thread;
		new_sock = malloc(1);
		*new_sock = new_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  fungsi , (void*) new_sock) < 0)
		{
			perror("could not create thread");
			return 1;
		}
		
		pthread_join( sniffer_thread , NULL);
		puts("Handler assigned");
	}
	
	if ((new_socket = accept(server_fd, (struct sockaddr *)&client, (socklen_t*)&addrlen))<0)
	{
		perror("accept failed");
		exit(EXIT_FAILURE);
	}
    return 0;
}

