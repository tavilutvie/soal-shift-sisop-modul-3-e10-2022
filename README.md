# SISOP Modul 3 (Kelompok E10)
**soal-shift-sisop-modul-3-E10-2022**

## Anggota Kelompok
No  | Nama                      |NRP
--- | ------------------------- | ------------
1   | Hafiz Kurniawan           | (5025201032)
2   | Eldenabih Tavirazin Lutvie| (5025201213)
3   | Michael Ariel Manihuruk   | (5025201158)

## Soal 1
<br>
1.	Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
<br>
<br>
(a.) Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
<br>
<br>
(b.)	Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
<br>
<br>
(c.)	Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
<br>
<br>
(d.)	Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
<br>
<br>
(e.)	Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
<br>

## Pengerjaan
Pertama-tama kita harus meakukan mapping menggunakan format base64. <br>
```c
char map_decode[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
char res[512];
pthread_t thread[12];
```
Kemudian yang harus dilakukan adalah membuat direktori file dengan nama `music`, selanjutnya membuat direktori bernama `quote`. Kedua direktori tersebut kemudian diunzip dalam bentuk `music.zip` dan `quote.zip` dengan menggunakan fungsi `file_download`.
```c
void *file_download(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, thread[0]))
  {
    int stat;
    if (fork() == 0)
    {
      char *cmd[] = {"mkdir", "-p", "music", NULL};
      execv("/usr/bin/mkdir", cmd);
    }
    else
    {
      while ((wait(&stat)) > 0)
        ;
      int stat2;

      if (fork() == 0)
      {
        char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/usr/bin/wget", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"unzip", "-q", "music.zip", "-d", "music", NULL};
        execv("/usr/bin/unzip", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;
    }
  }
  else if (pthread_equal(id, thread[1]))
  {

    int stat;
    if (fork() == 0)
    {
      char *cmd[] = {"mkdir", "-p", "quote", NULL};
      execv("/usr/bin/mkdir", cmd);
    }
    else
    {
      while ((wait(&stat)) > 0)
        ;
      int stat2;
      if (fork() == 0)
      {
        char *cmd[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/usr/bin/wget", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"unzip", "-q", "quote.zip", "-d", "quote", NULL};
        execv("/usr/bin/unzip", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;

      if (fork() == 0)
      {
        char *cmd[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", cmd);
      }

      while ((wait(&stat2)) > 0)
        ;
    }
  }
}
```
Setelah itu decode semua `file.txt` yang ada dengan menggunakan fungsi `decode` <br>
```c
void decode(char *txt)
{
  char buff[4], result[100];
  int i = 0, index = 0, count = 0, j;

  while (txt[i] != '\0')
  {
    for (j = 0; j < 64 && map_decode[j] != txt[i]; j++);
    buff[count++] = j;
    if (count == 4)
    {
      result[index++] = (buff[0] << 2) + (buff[1] >> 4);
      if (buff[2] != 64)
        result[index++] = (buff[1] << 4) + (buff[2] >> 2);
      if (buff[3] != 64)
        result[index++] = (buff[2] << 6) + buff[3];
      count = 0;
    }
    i++;
  }

  result[index] = '\0';
  strcpy(res, result);
```
Kemudian hasil decode tersebut akan disimpan dalam file `hasil` dengan menggunakan fungsi `decodefile` dan menghasilkan dua buah `file.txt` yakni `music.txt` dan `quote.txt`<br>
```c
void *decodefile(void *arg)
{
  pthread_t id = pthread_self();

  if (pthread_equal(id, thread[0]))
  {
    DIR *dir;
    struct dirent *d1;
    dir = opendir("./music/");

    while ((d1 = readdir(dir)) != NULL)
    {
      if (strstr(d1->d_name, ".txt"))
      {
        char file[150], loc[150], buffer[150];
        strcpy(file, "./music/");
        strcat(file, d1->d_name);
        FILE *hasil = fopen(file, "r");
        if (hasil == NULL)
            continue;
        fgets(buffer, 150, hasil);
        fclose(hasil);
        decode(buffer);
        FILE *file_hasil = fopen("./hasil/music.txt", "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
      }
    }
    closedir(dir);
  }
  else if (pthread_equal(id, thread[1]))
  {

    DIR *dir;
    struct dirent *d2;
    dir = opendir("./quote/");
    while ((d2 = readdir(dir)) != NULL)
    {
      if (strstr(d2->d_name, ".txt"))
      {
        char file[150];
        char loc[150], buffer[150];
        strcpy(file, "./quote/");
        strcat(file, d2->d_name);
        FILE *hasil = fopen(file, "r");
        if (hasil == NULL)
          continue;
        fgets(buffer, 150, hasil);
        fclose(hasil);
        decode(buffer);
        FILE *file_hasil = fopen("./hasil/quote.txt", "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
      }
    }
    closedir(dir);
  }
}
```
Lalu, kita menggunakan fungsi `addtozip` untuk melakukan unzip file `hasil` tersebut serta menambahkan `no.txt`. Dimana di fungsi ini terdapat dua buah thread, dimana thread 0 untuk ngeunzip `hasil.zip` dan thread 1 untuk nambahin `no.txt` di file `hasil`.
```c
void *addtozip(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, thread[0]))
  {
    if (fork() == 0)
    {
      char *cmd[] = {"unzip", "-P", "mihinomenestmilkyway", "-q", "hasil.zip", NULL};
      execv("/usr/bin/unzip", cmd);
    }
  }
  else if (pthread_equal(id, thread[1]))
  {
    FILE *file_no = fopen("./hasil/no.txt", "a");
    fprintf(file_no, "No");
    fclose(file_no);
  }
}
```
Terakhir, gunakan fungsi `filezip` untuk melakukan zip pada file `hasil` sehingga menghasilkan `hasil.zip` dimana untuk membuka isi file `hasil.zip` itu sendiri dibutuhkan autentikasi password dengan ketentuan password `mihinomenest[Nama user]`.
```c
void filezip()
{
  int stat;
  if (fork() == 0)
  {
    char *cmd[] = {"zip", "-P", "mihinomenestmilkyway", "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&stat)) > 0);
}
```


### Output
Berhasil mendownload `music.zip` dan `quote.zip` serta ngecreate `hasil.zip`. <br>

![image](soal1/gambar1.jpeg) <br>

Isi dari file `music` sebelum di decode adalah sebagai berikut. <br>

![image](soal1/gambar2.jpeg) <br>

Isi dari file `quote` sebelum di decode adalah sebagai berikut. <br>

![image](soal1/gambar3.jpeg) <br>

Isi dari file `hasil` adalah sebagai berikut. <br>

![image](soal1/gambar4.jpeg) <br>

Isi dari file `music.txt` adalah sebagai berikut. <br>

![image](soal1/gambar5.jpeg) <br>

Isi dari file `no.txt` adalah sebagai berikut. <br>

![image](soal1/gambar6.jpeg) <br>

Isi dari file `quote.txt` adalah sebagai berikut. <br>

![image](soal1/gambar7.jpeg) <br>

Berhasil menampilkan autentikasi menggunakan password saat membuka file `hasil.zip`. <br>

![image](soal1/gambar8.jpeg) <br>

Berhasil melakukan autentifikasi password dan dapat melihat isi dari file `hasil.zip`. <br>

![image](soal1/gambar9.jpeg) <br>

### Kendala
Tidak ada kendala.


# Soal 2 (Belum Selesai)
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria.
## Cara Pengerjaan
### 2a. <br>
**Register dan Login**<br>
Pada client.c, register dilakukan dengan menggunakan potongan kode berikut.<br>
```c
valread = read(sock, buffer, 1024);
printf("%s", buffer);
scanf(" %[^\n]s", username);

if(checkusername(username)==1){
    printf("Username already exists\n");
    break;
}
else{
    write(sock, username, 1024);
    valread = read(sock, buffer, 1024);
    while(1){
        printf("%s", buffer); 
        scanf(" %[^\n]s", password);

        int i=0;
        while(i < strlen(password)) {
            if(password[i] >= '0' && password[i] <= '9') valid1 = true;
            if(password[i] >= 'A' && password[i] <= 'Z') valid2 = true;
            if(password[i] >= 'a' && password[i] <= 'z') valid3 = true;
            i++;
        }

        if(strlen(password)>5 && valid1 && valid2 && valid3){
            valid1=false;
            valid2=false;
            valid3=false;
            break;
        }
        else{
            valid1=false;
            valid2=false;
            valid3=false;                
        }
    }
    write(sock, password, 1024);
}
```
Fungsi checkusername() digunakan untuk mengecek adanya kesamaan username pada users.txt.<br>
Kriteria password diterapkan dengan menggunakan potongan kode berikut.
```c
while(i < strlen(password)) {
    if(password[i] >= '0' && password[i] <= '9') valid1 = true;
    if(password[i] >= 'A' && password[i] <= 'Z') valid2 = true;
    if(password[i] >= 'a' && password[i] <= 'z') valid3 = true;
    i++;
}
```
Pada client.c, login dilakukan dengan menggunakan potongan kode berikut.<br>
```c
while (1){
char ver[1024] = {0};
while (strcmp(ver, "") == 0)
{
    bzero(buffer, 1024);
    valread = read(sock, buffer, 1024);
    printf("%s", buffer);
    scanf(" %[^\n]s", username);
    write(sock, username, 1024);

    bzero(buffer, 1024);
    valread = read(sock, buffer, 1024);
    printf("%s", buffer); 
    scanf(" %[^\n]s", password);
    write(sock, password, 1024);

    valread = read(sock, ver, 1024);
}
while (!strcmp(ver, "1")){
    bzero(buffer, 1024);    
    valread = read(sock, buffer, 1024);
    printf("%s", buffer);
    bzero(buffer, 1024);
    scanf(" %[^\n]s", input);
    write(sock, input, 1024);
    if (!strcmp(input, "add")){
        return 0;
    }
}
```
Variabel ver merupakan indikasi keberhasilan login.<br>
Pada server.c, register dilakukan dengan menggunakan potongan kode berikut.<br>
```c
FILE* fp = fopen("users.txt", "a");
sprintf(menu, "Enter username: \e[s\n\e[u");
write(sock, menu, 1024); 
bzero(menu, 1024);
valread = read(sock, username, 1024);
if (valread < 1) return 0;

sprintf(menu, "Enter password : \e[s\n\e[u");
write(sock, menu, 1024); 
bzero(menu, 1024);
valread = read(sock, password, 1024);
if (valread < 1) return 0;

fprintf(fp, "%s:%s\n", username, password);
fclose(fp);
```
Pada kode di atas file users.txt dibuka dan diisi data username dan password yang didapatkan dari client.<br>
Pada server.c, login dilakukan dengan menggunakan potongan kode berikut.<br>
```c
char* ver = "0";
FILE* fp = fopen("users.txt", "r+");
while (ver == "0"){
    bzero(menu, 1024);
    sprintf(menu, "Enter username : \e[s\n\e[u");
    int ler = write(sock, menu, 1024);
    bzero(menu, 1024);
    valread = read(sock, username, 1024);
    if (valread < 1) return 0;

    sprintf(menu, "Enter password : \e[s\n\e[u");
    write(sock, menu, 1024); 
    bzero(menu, 1024);
    valread = read(sock, password, 1024); 
    if (valread < 1) return 0;

    while (fgets(buffer, 1024, fp) != NULL && ver == "0") 
    {
        char f_username[1024], f_password[1024];
        char *token = strtok(buffer, ":");
        strcpy(f_username, token);
        token = strtok(NULL, "\n");
        strcpy(f_password, token);
        
        if (strcmp(username, f_username) == 0 && strcmp(password, f_password) == 0)
        {
            printf("Logged in\n");
            ver = "1";
            write(sock, ver, 1024);
        }
    }
```
Pada kode di atas dilakukan pengecekan data sesuai dengan data pada users.txt.

### 2b. <br>
**Membuat problems.tsv**<br>
Dilakukan dengan potongan kode berikut pada server.c.
```c
FILE* fp;
fp = fopen("problems.tsv", "a");
fclose(fp);
```

## Output
![image](/uploads/c89cafed8c691c1f0fd67192f951f7eb/image.png) <br>
![image](/uploads/90d13e8eedbb591b5854f9bd4b2efcfc/image.png) <br>
![image](/uploads/819c21b94f8c25bea77b46a2ffd11343/image.png) <br>
![image](/uploads/932dfe3a6ca0c606d1428c567ab8931a/image.png) <br>
![image](/uploads/dfe701d913468569318db044a6b24724/image.png) <br>

## Kendala
Belum paham mengenai socket dan thread.


# Soal 3
___
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang,
Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan
jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan,
Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan: <br>
```
#Program soal3 terletak di /home/[user]/shift3/hartakarun
$./soal3
#Hasilnya adalah sebagai berikut
/home/[user]/shift3/hartakarun
		|-jpg
				|--file1.jpg
				|--file2.jpg
				|--file3.jpg
		|-c
				|--file1.c
		|-tar.gz
				|--file1.tar.gz
```

### a. <br>
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan
ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan
berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada
file yang tertinggal, program harus mengkategorikan seluruh file pada working
directory secara rekursif <br>
### b.  <br>
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki
ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder
“Hidden".<br>
### c.  <br>
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan
dioperasikan oleh 1 thread.<br>
### d. <br>
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program
client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama
“hartakarun.zip” ke working directory dari program client.
### e.  <br>
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan
command berikut ke server<br>

> send hartakarun.zip

Karena Nami tidak bisa membuat programnya, maka Nami meminta bantuanmu untuk
membuat programnya. Bantulah Nami agar programnya dapat berjalan! <br>
Catatan: <br>
● Kategori folder tidak dibuat secara manual, harus melalui program C <br>
● Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama <br>
● Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”) <br>
● Dilarang juga menggunakan fork, exec dan system() <br>
Struktur Direktori Client Server :
```
	├── Client
					├── client.c
					└── hartakarun.zip
	└── Server
					├── server.c
					└── hartakarun.zip
```

## Penyelesaian
### Soal 3.a
Melakukan extract zip file `hartakarup.zip`, berhubung dilarang menggunakan fork, exec, dan system. Maka dapat memanfaatkan fitur `popen` untuk melakukan extract / unzip file. <br>
```c
 FILE * file;
    char buff[102400];
    int read;

    memset(buff, '\0', sizeof(buff));
    file = popen("unzip -o /home/hafizen/shift3/hartakarun.zip", "r");

    if( file != NULL){
        read = fread(buff, sizeof(char), 102400 - 1, file);
    }

    if (read > 0){
        printf("%s\n", buff);
    }
    pclose(file);
```
Kemudian directory program akan berpindah ke folder hasil extract zip file `hartakarun` dapat dengan menggunakan fasilitas `chdir()`.
```c
 if(chdir("./hartakarun") < 0){
        exit(EXIT_FAILURE);
    }
```
Melakukan kategorisasi file didalam folder hartakarun secara rekursif dimana program pada fungsi `rekursif()` dengan menerima parameter direktory folder yang berjalan `curr_dir` dimana diperoleh dari `getcwd`.
```c
rekursif(curr_dir);
```
```c
void rekursif(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir){
        remove(basePath);
        return;
    }
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }

            rekursif(path);
        }
    }
    closedir(dir);
}
```
##  Soal 3.b
Melakukan kategorisasi file berdasarkan ekstensi file, atau bila diawali dengan `.` maka termasuk hidden file sehingga masuk ke folder `Hidden`, bila tidak memiliki ekstensi masuk ke folder `Unknown`.  <br>
Jika file termasuk hidden file. <br>
```c
 if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
```
Jika file memiliki ekstensi, Karena kategorisasi file juga case insensitive maka nama file dapat dicek dengan konstistensi dimana diseragamkan dengan karakter lowercase dimana dioperasikan dengan fungsi `tolower()`. <br>
```c
 else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
```
Untuk file yang tidak berekstensi <br>
```c
  else
    {
        strcpy(dirname, "Unknown");
    }
```
### Soal 3.c
Proses kategorisasi dengan menggunakan 1 thread untuk setiap filenya
```c
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
```

### Soal 3.d
Program client - server socket programming untuk melakukan transfer file dimana dalam program ini menggunakan socket TCP/IP programming. <br><br>
Sebelum dilakukan transfer folder hartakarun akan dilakukan zip file dahulu dimana dalam program memanfaatkan fungsi `popen()` ke directory `client`. zip file dilakukan dengan kembali ke directory `shift3` kemudian dilakukan zip folder `hartakarun`.
```c
if(chdir("..") < 0){
		exit(EXIT_FAILURE);
	}
	
	FILE * file;
    char buff[100];
    int read;

    memset(buff, '\0', sizeof(buff));
    file = popen("zip -o -r /home/hafizen/shift3/client/hartakarun.zip hartakarun", "w");

    if( file != NULL){
        read = fread(buff, sizeof(char), 100, file);
    }

    if (read > 0){
        printf("%s\n", buff);
    }
    pclose(file);
```
Membuat koneksi antara klien-server socket programming. <br><br>
Sisi Client:
```c
#define PORT_NUMBER     8080
#define SERVER_ADDRESS  "127.0.0.1"

int main(int argc, char **argv)
{
        int client_socket;
        int peer_socket;
        socklen_t       sock_len;
        ssize_t len;
        struct sockaddr_in      client_addr;
        struct sockaddr_in      peer_addr;
        int fd;
        int sent_bytes = 0;
        char file_size[256];
        struct stat file_stat;
        off_t offset;
        int remain_data;

        /* Create client socket */
        client_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (client_socket == -1)
        {
                fprintf(stderr, "Error creating socket --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }

        /* Zeroing client_addr struct */
        memset(&client_addr, 0, sizeof(client_addr));
        /* Construct client_addr struct */
        client_addr.sin_family = AF_INET;
        inet_pton(AF_INET, SERVER_ADDRESS, &(client_addr.sin_addr));
        client_addr.sin_port = htons(PORT_NUMBER);

        /* Bind */
        if ((bind(client_socket, (struct sockaddr *)&client_addr, sizeof(struct sockaddr))) == -1)
        {
                fprintf(stderr, "Error on bind --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }

        /* Listening to incoming connections */
        if ((listen(client_socket, 5)) == -1)
        {
                fprintf(stderr, "Error on listen --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }

```
<br>
Sisi Server:

```c
#define PORT_NUMBER     8080
#define SERVER_ADDRESS  "127.0.0.1"
#define FILENAME        "hartakarun.zip"
int main(int argc, char **argv)
{
        int server_socket;
        ssize_t len;
        struct sockaddr_in server_addr;
        char buffer[BUFSIZ];
        int file_size;
        FILE *received_file;
        int remain_data = 0;

        /* Zeroing server_addr struct */
        memset(&server_addr, 0, sizeof(server_addr));

        /* Construct server_addr struct */
        server_addr.sin_family = AF_INET;
        inet_pton(AF_INET, SERVER_ADDRESS, &(server_addr.sin_addr));
        server_addr.sin_port = htons(PORT_NUMBER);

        /* Create server socket */
        server_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (server_socket == -1)
        {
                fprintf(stderr, "Error creating socket --> %s\n", strerror(errno));

                exit(EXIT_FAILURE);
        }

        /* Connect to the client */
        if (connect(server_socket, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1)
        {
                fprintf(stderr, "Error on connect --> %s\n", strerror(errno));

                exit(EXIT_FAILURE);
        }

```
### Soal 3.e
Melakukan transfer file `hartakarun.zip` dari client ke server socket programming. <br>
<br>
Sisi Client mengirim file:
```c
chdir("../client");

        fd = open("hartakarun.zip", O_RDONLY);
        if (fd == -1)
        {
                fprintf(stderr, "Error opening file --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }

        /* Get file stats */
        if (fstat(fd, &file_stat) < 0)
        {
                fprintf(stderr, "Error fstat --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }

        //fprintf(stdout, "File Size: \n%zd bytes\n", file_stat.st_size);

        sock_len = sizeof(struct sockaddr_in);
        /* Accepting incoming peers */
        peer_socket = accept(client_socket, (struct sockaddr *)&peer_addr, &sock_len);
        if (peer_socket == -1)
        {
                fprintf(stderr, "Error on accept --> %s", strerror(errno));

                exit(EXIT_FAILURE);
        }
        fprintf(stdout, "Accept peer --> %s\n", inet_ntoa(peer_addr.sin_addr));

        sprintf(file_size, "%lu", file_stat.st_size);

        /* Sending file size */
        len = send(peer_socket, file_size, sizeof(file_size), 0);
        if (len < 0)
        {
              fprintf(stderr, "Error on sending greetings --> %s", strerror(errno));

              exit(EXIT_FAILURE);
        }

        fprintf(stdout, "Client sent %zd bytes for the size\n", len);

        offset = 0;
        remain_data = file_stat.st_size;
        /* Sending file data */
        while (((sent_bytes = sendfile(peer_socket, fd, &offset, BUFSIZ)) > 0) && (remain_data > 0))
        {
                //fprintf(stdout, "1. Server sent %d bytes from file's data, offset is now : %zd and remaining data = %d\n", sent_bytes, offset, remain_data);
                remain_data -= sent_bytes;
                //fprintf(stdout, "2. Server sent %d bytes from file's data, offset is now : %zd and remaining data = %d\n", sent_bytes, offset, remain_data);
        }

        close(peer_socket);
        close(client_socket);

        return 0;
}
```
<br>
Sisi Server menerima file: <br>

```c
/* Receiving file size */
        recv(server_socket, buffer, BUFSIZ, 0);
        file_size = atoi(buffer);
        fprintf(stdout, "\nFile size : %d\n", file_size);

        received_file = fopen(FILENAME, "w");
        if (received_file == NULL)
        {
                fprintf(stderr, "Failed to open file --> %s\n", strerror(errno));

                exit(EXIT_FAILURE);
        }

        remain_data = file_size;
        while ((remain_data > 0) && ((len = recv(server_socket, buffer, BUFSIZ, 0)) > 0))
        {
                fwrite(buffer, sizeof(char), len, received_file);
                fprintf(stdout, "Receive %lu bytes and we hope :- %d bytes\n", len, remain_data);
                remain_data -= len;
        }
        fclose(received_file);

        close(server_socket);

        return 0;
}
```

<br>
## Screenshot <br>
Tampilan hasil extract file hartakarun.zip sebelum dilakukan kategorisasi. <br>
![301](/uploads/42fa9dbb59f984a30e7f78d4a84aefba/301.png)
<br>
Tampilan setelah dilakukan kategorisasi. <br>
![302](/uploads/94cf7142e9b6038c8bc3f337aca5606f/302.png)
<br>
Tampilan direktori pada sisi client-server socket programming. <br>
![303](/uploads/683a80ff6a19b78bb9ec13484a94bbb7/303.png)
<br>

## Kendala
* Hasil transfer file zip `hartakarun.zip` ke server mengalami corrupt file bila dilakukan extract.
